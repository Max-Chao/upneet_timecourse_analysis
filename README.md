#Deconvolution analysis for Ivashkiv Lab knee surgery Timecourse


-scripts include four scripts:
1. new_upneet_knee_timecourse.R: scripts used to generate the files from a cell type database. Some functions included at the bottom are older versions of functions going to be added to PanglaoSCDC. Conditions is 100 iterations of 250 randomly sampled cells. Cell types were selected for any cells that could exist within the knee and filtered for cell types that had at least 300 cells in the database. Removed mt and ribosomal genes before performing basis matrix calculations.
2. old_funcs_PanglaoSCDC.R old and now defunct functions from WIP project PanglaoSCDC which uses PanglaoDB to randomly sample for cells for a cell type and bootstrap test these references generated through SCDC to define cell type proportions in a bulk RNAseq sample. 
3. upneet_lineplot.R: script to generate the visualize the plots by taking the top 12 expressing cell types from deconvolution, finding the median across the 100 iterations, and plotting along the timecourse.
4. ggRadar_upneet.R: script to visualize deconvolution by looking at it through a ggRadar plot

data input: 
scdc_full_nonpoly_rm.rds: full scdc output when running scdc, including basis matrices and cell type deconvolution results, final output of the new_upneet_knee_timecourse.R and inputs for upneet_lineplot.R and ggRadar_upneet.R


